import json
import requests
import base64

class SampleClient:
    def __init__(self):
        self.url = 'http://siam.dev.abctech-thailand.com/candidate/rest/candidateService/'
        self.view = 'http://siam.dev.abctech-thailand.com/candidate/view.html'
        self.headers = {'content-type': 'application/json'}

    def abc_save(self,data):
        try :
            r = requests.post(self.url+'save.json',data=json.dumps(data),headers=self.headers)
            rx = r.text
        except Exception,ex:
            rx = json.dumps({"status":-1,"errorMessage":str(ex.message)})
        return json.loads(rx)

    def abc_load(self,uuid):
        try :
            r = requests.get(self.url+'load/'+uuid+'.json')
            rx = r.text
        except Exception,ex:
            rx = json.dumps({"status":-1,"errorMessage":str(ex.message)})
        return json.loads(rx)
            
    def abc_upload(self,uuid,image):
        try :
            r = requests.put(self.url+'upload/image/'+uuid+'.json',data=json.dumps(image),headers=self.headers)
            rx = r.text
        except Exception,ex:
            rx = json.dumps({"status":-1,"errorMessage":str(ex.message)})
        return json.loads(rx)

    def abc_view(self,uuid):
        try :
            r = requests.get(self.view+'?uuid='+uuid)
            rx = r.text
        except Exception,ex:
            rx = ex.message
        return rx

class User:
    def __init__(self,name,age,gender,image):
        self.name = name
        self.age = age
        self.gender = gender
        self.uuid = ''
        self.image = ''
        self.convert_image(image)

    def set_uuid(self,uuid):
        self.uuid = str(uuid)

    def get_data(self):
        return {"name":self.name,"age":self.age,"gender":self.gender}

    def get_uuid(self):
        return self.uuid

    def get_image(self):
        return {"uuid":self.uuid,"imageBase64":self.image}

    def convert_image(self,image):
        try :
            with open(image,"rb") as image_file:
                self.image = base64.b64encode(image_file.read())
        except Exception,ex:
            self.image = "None"    

class Utils:
    def write_file(self,data):
        try :
            f = open("output.html","w")
            f.write(data)
            f.close()
        except Exception,ex:
            print ex.message













