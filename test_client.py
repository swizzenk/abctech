from client import *
import unittest

class TestClient (unittest.TestCase):
    def setUp(self):
        self.client = SampleClient()
    def testOne(self):
        self.client.abc_save({'gender': 'M', 'age': 30, 'name': 'Natthasun Jirakaewkant'})
    def testTwo(self):
        self.client.abc_load("36e8725b-8de7-4327-b27b-a4576d733740")
    def testThree(self):
        self.client.abc_upload("36e8725b-8de7-4327-b27b-a4576d733740",{'uuid': '4fed4279-3296-46a3-9efe-803f820956f1', 'imageBase64': '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEB'})
    def testFour(self):
        self.client.abc_view("36e8725b-8de7-4327-b27b-a4576d733740")
    def tearDown(self):
        self.client = None


def main():
    unittest.main()

if __name__ == '__main__':
    main()
