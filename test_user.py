from client import *
import unittest

class TestUser(unittest.TestCase):
    def setUp(self):
        self.user = User("Natthasun Jirakaewkant",30,"M","natthasun.jpg")
    def testOne(self):
        self.user.set_uuid("155121-51251221-45132231")
    def testTwo(self):
        self.user.get_data()
    def testThree(self):
        self.user.get_image()
    def testFour(self):
        self.user.convert_image("natthasun.jpg")
    def testFive(self):
        self.user.get_uuid()
    def tearDown(self):
        self.ut = None


def main():
    unittest.main()

if __name__ == '__main__':
    main()
