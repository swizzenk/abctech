from client import *
import unittest

class CanWriteFile(unittest.TestCase):
    def setUp(self):
        self.ut = Utils()
    def testOne(self):
        self.ut.write_file("testOne")
    def testTwo(self):
        self.ut.write_file("testTwo")
    def testThree(self):
        self.ut.write_file("testThree")
    def tearDown(self):
        self.ut = None


def main():
    unittest.main()

if __name__ == '__main__':
    main()
